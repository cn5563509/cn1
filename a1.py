import os
import time
import networkx as nx
import pandas as pd
from concurrent.futures import ProcessPoolExecutor


# Modified function to include timing
def parallel_descriptor_calculation(graph_tuple):
    start_time = time.time()  # Capture start time

    file, graph = graph_tuple
    descriptors = calculate_descriptors(graph)

    end_time = time.time()  # Capture end time
    duration = end_time - start_time  # Calculate duration

    return [file] + descriptors + [duration]  # Return original results with duration


# Function to calculate structural descriptors for a given network
def calculate_descriptors(graph):
    # Convert multi graph to simple graph
    graph = nx.Graph(graph)

    num_nodes = graph.number_of_nodes()
    num_edges = graph.number_of_edges()
    min_degree = min(dict(graph.degree()).values())
    max_degree = max(dict(graph.degree()).values())
    avg_degree = sum(dict(graph.degree()).values()) / num_nodes
    avg_clustering_coefficient = nx.average_clustering(graph)
    assortativity = nx.degree_assortativity_coefficient(graph)
    avg_path_length = nx.average_shortest_path_length(graph)
    diameter = nx.diameter(graph)

    return [num_nodes, num_edges, min_degree, max_degree, avg_degree, avg_clustering_coefficient, assortativity,
            avg_path_length, diameter]


# Path to the directory containing network files
if __name__ == "__main__":
    directory = r'./source/A1-networks/'
    graph_data = []

    for subdir, dirs, files in os.walk(directory):
        for file in files:
            if file.endswith('.net'):
                file_path = os.path.join(subdir, file)
                graph = nx.read_pajek(file_path)
                graph_data.append((file, graph))

    # Use ProcessPoolExecutor to calculate descriptors in parallel
    with ProcessPoolExecutor(max_workers=12) as executor:
        results = list(executor.map(parallel_descriptor_calculation, graph_data))

    # Modify DataFrame columns to include 'Duration'
    df = pd.DataFrame(results, columns=['Network', 'NumNodes', 'NumEdges', 'MinDegree', 'MaxDegree', 'AvgDegree',
                                        'AvgClusteringCoefficient', 'Assortativity', 'AvgPathLength', 'Diameter',
                                        'Duration'])

    print(df)
