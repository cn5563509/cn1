import networkx as nx
import matplotlib.pyplot as plt

unweighted_file = 'primaryschool_u.net'
weighted_file = 'primaryschool_w.net'

metadata_file = 'metadata_primary_school.txt'

G_unweighted = nx.read_pajek(unweighted_file)
G_weighted = nx.read_pajek(weighted_file)

with open(metadata_file, 'r') as f:
    next(f)
    metadata_lines = f.readlines()

node_to_group = {}
for line in metadata_lines:
    node_id, group = line.strip().split()
    node_to_group[int(node_id)] = group

communities_unweighted_str = nx.algorithms.community.greedy_modularity_communities(G_unweighted)
communities_weighted_str = nx.algorithms.community.greedy_modularity_communities(G_weighted, weight="weight")

communities_unweighted = []
# cast every elemnt to int
for original_set in communities_unweighted_str:
    converted_set = frozenset(int(element) for element in original_set)
    communities_unweighted.append(converted_set)
communities_weighted = []
# cast every elemnt to int
for original_set in communities_weighted_str:
    converted_set = frozenset(int(element) for element in original_set)
    communities_weighted.append(converted_set)


# Visualization
#inverse_weights = {edge: 1 / G_weighted[edge[0]][edge[1]][0]['weight'] for edge in G_weighted.edges()}
#pos = nx.kamada_kawai_layout(G_weighted, dist=inverse_weights)
pos = nx.spring_layout(G_weighted, weight='myweight')


# UNWEIGHTED BAR PLOT
community_group_counts = {}
for i, community in enumerate(communities_unweighted):
    community_group_counts[i] = {}
    for node in community:
        group = node_to_group[int(node)]
        community_group_counts[i][group] = community_group_counts[i].get(group, 0) + 1
# stacked bar plot
group_labels = list(set(node_to_group.values()))
num_communities = len(communities_unweighted)

fig, ax = plt.subplots(figsize=(10, 6))
for i in range(num_communities):
    counts = [community_group_counts[i].get(group, 0) for group in group_labels]
    ax.bar(group_labels, counts, label=f"Community {i + 1}")

ax.set_xlabel("School Groups")
ax.set_ylabel("Number of Individuals")
ax.set_title("Composition of Communities by School Groups")
ax.legend()

plt.savefig("stacked_bar_unweighted.png")

# WEIGHTED BAR PLOT
community_group_counts = {}
for i, community in enumerate(communities_weighted):
    community_group_counts[i] = {}
    for node in community:
        group = node_to_group[node]
        community_group_counts[i][group] = community_group_counts[i].get(group, 0) + 1
# stacked bar plot
group_labels = list(set(node_to_group.values()))
num_communities = len(communities_weighted)

fig, ax = plt.subplots(figsize=(10, 6))
for i in range(num_communities):
    counts = [community_group_counts[i].get(group, 0) for group in group_labels]
    ax.bar(group_labels, counts, label=f"Community {i + 1}")

ax.set_xlabel("School Groups")
ax.set_ylabel("Number of Individuals")
ax.set_title("Composition of Communities by School Groups")
ax.legend()

plt.savefig("stacked_bar_weighted.png")



def assign_community_colors(G, communities):
    num_communities = len(communities)
    cmap = plt.get_cmap('tab20', num_communities)
    community_colors = []
    for node in list(G.nodes):
        #print(lookup_color(node, communities, cmap))
        community_colors.append(lookup_color(node, communities, cmap))
    return community_colors

def lookup_color(node, communities, cmap):
    for i, community in enumerate(communities):
        color = cmap(i)
        if int(node) in community:
            return color


node_colors_unweighted = assign_community_colors(G_unweighted, communities_unweighted)
node_colors_weighted = assign_community_colors(G_weighted, communities_weighted)
# Plot unweighted network
plt.figure(figsize=(10, 6))
nx.draw(G_unweighted, pos, node_color=node_colors_unweighted, node_size=50, with_labels=False)
plt.title("Unweighted Network Communities")
plt.savefig("unweighted.png")


# Plot weighted network
plt.figure(figsize=(10, 6))
nx.draw(G_weighted, pos, node_color=node_colors_weighted, node_size=50, with_labels=False)
plt.title("Weighted Network Communities")
plt.savefig("weighted.png")


# communities regarding of school groups
def count_school_groups(community):
    group_counts = {}
    for node in community:
        group = node_to_group.get(node, 'Unknown')
        group_counts[group] = group_counts.get(group, 0) + 1
    return group_counts


community_dict_unweighted = {} 
for i, community in enumerate(communities_unweighted):
    print(f"Community {i + 1}:")
    group_counts = count_school_groups(community)
    for group, count in group_counts.items():
        print(f"{group}: {count} individuals")
    
    community_dict_unweighted[community] = i + 1

community_dict_weighted = {} 
for i, community in enumerate(communities_weighted):
    print(f"Community {i + 1}:")
    group_counts = count_school_groups(community)
    for group, count in group_counts.items():
        print(f"{group}: {count} individuals")
    
    community_dict_weighted[community] = i + 1

with open("unweighted.clu", "w") as pajek_file:
    pajek_file.write("*Vertices {}\n".format(len(G_unweighted.nodes)))
    for node in G_unweighted.nodes:
        for i, community in enumerate(communities_unweighted):
            if int(node) in community:
                pajek_file.write(f"{node} \"community_{community_dict_unweighted[community]}\"\n")

    pajek_file.write("*arcs\n")
    for edge in G_unweighted.edges:
        pajek_file.write("{} {}\n".format(edge[0], edge[1]))


with open("weighted.clu", "w") as pajek_file:
    # Write node definitions
    pajek_file.write("*Vertices {}\n".format(len(G_weighted.nodes)))
    for node in G_weighted.nodes:
        for i, community in enumerate(communities_weighted):
            if int(node) in community:
                pajek_file.write(f"{node} \"community_{community_dict_weighted[community]}\"\n")

    pajek_file.write("*arcs\n")
    for edge in G_weighted.edges:
        pajek_file.write("{} {}\n".format(edge[0], edge[1]))