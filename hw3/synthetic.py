import os
import networkx as nx
import matplotlib.pyplot as plt
import infomap
from networkx.algorithms.community import modularity
from sklearn.metrics.cluster import normalized_mutual_info_score, adjusted_mutual_info_score
from networkx.algorithms.community import greedy_modularity_communities
from pathlib import Path
import community

# Function to load the synthetic network
def load_network(full_path):
    # Create a new graph
    G = nx.Graph()

    # Open and read the file
    with open(full_path, 'r') as file:
        mode = None  # Track whether we're reading vertices or edges

        for line in file:
            if line.startswith('*'):  # Check what section we are in
                if 'vertices' in line:
                    mode = 'vertices'
                elif 'edges' in line:
                    mode = 'edges'
                continue

            if mode == 'vertices':
                # Parse vertex lines: "<index> <label> <x> <y> <shape>"
                parts = line.split()
                if len(parts) < 5:
                    continue  # Skip malformed lines
                index, label, x, y, shape = parts
                # Add vertex to graph
                G.add_node(index, label=label, pos=(float(x), float(y)), shape=shape)

            elif mode == 'edges':
                # Parse edge lines: "<from> <to> <weight>"
                parts = line.split()
                if len(parts) < 3:
                    continue  # Skip malformed lines
                source, target, weight = parts
                # Add edge to graph
                G.add_edge(source, target, weight=float(weight))

    return G

def visualize_network(G, partition, pos, title,filename):
    communities = set(partition.values())
    cmap = plt.cm.get_cmap('jet', len(communities))
    plt.figure(figsize=(10, 6))
    for i, com in enumerate(communities):
        nx.draw_networkx_nodes(G, pos, nodelist=[node for node, com_id in partition.items() if com_id == com], 
                               node_color=cmap(i), alpha=0.5)
    nx.draw_networkx_edges(G, pos, alpha=0.5)
    plt.title(title)
    plt.colorbar(label='Community')
    plt.axis('off')
    plt.savefig(f'community_structure_prr_{prr}.png')

# Function to apply community detection algorithms
def detect_communities(G, algorithm):
    if algorithm == 'infomap':
        im = infomap.Infomap()
        for source, target, data in G.edges(data=True):
            weight = data.get('weight', 1.0)  
            im.add_link(int(source), int(target), weight)
        im.run()
        communities = [im.get_modules()]
    elif algorithm == 'greedy_modularity':
        communities = list(greedy_modularity_communities(G))
    elif algorithm == 'louvain':
        communities = list(nx.algorithms.community.label_propagation.label_propagation_communities(G))
    return communities

def jaccard_coefficient(set1, set2):
    intersection = len(set1.intersection(set2))
    union = len(set1.union(set2))
    return intersection / union

# Function to compute evaluation metrics
def evaluate_partition(true_partition, detected_partition):
    true_partition_set = {tuple(inner_list) for inner_list in true_partition} 
    detected_partition_set = {tuple(inner_list) for inner_list in detected_partition}
    jaccard_index = jaccard_coefficient(true_partition_set, detected_partition_set)
    #nmi = normalized_mutual_info_score(true_partition, detected_partition)
    #nvi = adjusted_mutual_info_score(true_partition, detected_partition)
    nmi=0
    nvi=0
    return jaccard_index, nmi, nvi

# Function to convert communities to partition format
def communities_to_partition(communities):
    partition = []
    for community in communities:
        partition.append(set(community))
    return partition

if __name__ == '__main__':
    # Parameters
    N = 300
    nblocks = 5
    prs = 0.02
    prr_values = [0, 0.16, 1.0]
    algorithms = ['infomap', 'greedy_modularity', 'louvain']
    #algorithms = ['greedy_modularity', 'louvain']

    # Track the evolution of number of communities and modularity
    num_communities = {alg: [] for alg in algorithms}
    modularity_scores = {alg: [] for alg in algorithms}

    # Load and analyze networks for different prr values
    for prr in prr_values:
        #for i in range(51):
            # filename = f'synthetic_network_N_{N}_blocks_{nblocks}_prr_{prr}_prs_{prs}.net'
            breakpoint
            #current_dir = Path.cwd()
            filename = 'synthetic_network_N_300_blocks_5_prr_0.00_prs_0.02.net'
            

            current_directory = os.getcwd()
            files_directory = os.path.join(current_directory, "A3_synthetic_networks")
            # Get a list of all files in the current directory
            files_in_directory = os.listdir(files_directory)
        #    for filename in files_in_directory:
                
            # Skip the file named "synthetic.py"
            if filename == filename:
                if filename == "synthetic.py":
                    continue
                current_dir = os.path.dirname(os.path.abspath(__file__))
                full_path = os.path.join(current_dir, filename)
                print(f'File path: {full_path}')
                G = load_network(full_path)
                pos_ref = nx.kamada_kawai_layout(G)
                print(f'Network loaded: {G.number_of_nodes()} nodes, {G.number_of_edges()} edges')
                # True community structure
                true_communities = [list(range(i * 60, (i + 1) * 60)) for i in range(nblocks)]

                for algorithm in algorithms:
                # Community detection
                    communities = detect_communities(G, algorithm)
                    communities1=communities
                    print(f'Comunities'%communities)

                    num_communities[algorithm].append(len(communities))
                    partition = [set(community) for community in communities]
        
            # Evaluate modularity using the partition
                    if algorithm == 'infomap':
                        largest_value = max(communities[0].values())
                        new_communities = [frozenset({str(key) for key, value in communities[0].items() if value == curr_value}) for curr_value in range(1, largest_value)]
                        some_values = frozenset(range(1,301)) - frozenset([int(el) for el in frozenset.union(*new_communities)])
                        new_communities[0] = frozenset(new_communities[0] | frozenset([str(el) for el in some_values]))
                        communities = new_communities
                    modularity_score = modularity(G, communities)
        
            # Append modularity score to the list
                    modularity_scores[algorithm].append(modularity_score)


            # Evaluate partition
                    jaccard_index, nmi, nvi = evaluate_partition(true_communities, communities)
                    print(f'{algorithm} (prr={prr}): Jaccard Index={jaccard_index}, NMI={nmi}, NVI={nvi}')
                    

                    #visualize_network(G, communities1[0], pos_ref, f'{algorithm} (prr={prr})', filename)
                    pos = nx.spring_layout(G)  # compute graph layout
                    plt.figure(figsize=(10, 10))
                    plt.axis('off')
                    nx.draw_networkx_nodes(G, pos, node_size=300, cmap=plt.cm.RdYlBu, node_color=list(communities1.values()))
                    nx.draw_networkx_edges(G, pos, alpha=0.3)
                    plt.show(G)
                    


        # Visualization for selected prr values
        
           

    # Plotting the evolution of number of communities and modularity
    plt.figure(figsize=(12, 6))
    for algorithm in algorithms:
        plt.plot(prr_values, num_communities[algorithm], label=f'{algorithm}')
    plt.xlabel('prr')
    plt.ylabel('Number of Communities')
    plt.title('Evolution of Number of Communities')
    plt.legend()
    plt.show()

    plt.figure(figsize=(12, 6))
    for algorithm in algorithms:
        plt.plot(prr_values, modularity_scores[algorithm], label=f'{algorithm}')
    plt.xlabel('prr')
    plt.ylabel('Modularity')
    plt.title('Evolution of Modularity')
    plt.legend()
    plt.show()