from typing import Set, List

import networkx as nx
from networkx import Graph


def louvain_detect_communities(graph: Graph) -> List[Set[int]]:
    communities = nx.algorithms.community.louvain_communities(graph, seed=42)
    return communities
