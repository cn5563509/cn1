from typing import Set, List

import networkx as nx
from networkx import Graph


def greedy_modularity_detect_communities(graph: Graph) -> List[Set[int]]:
    # This function finds communities in a graph using the Clauset-Newman-Moore greedy modularity maximization.
    communities = list(nx.algorithms.community.greedy_modularity_communities(graph))
    return communities
