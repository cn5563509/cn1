import os
import re

import matplotlib.pyplot as plt
from networkx.algorithms.community import modularity

from synthetic_alg_greedy import *
from synthetic_alg_infomap import *
from synthetic_alg_louvain import *
from synthetic_load import load_network
from synthetic_metrics import *


def calculate_community_modularity(graph: Graph, communities: List[Set[int]]) -> float:
    # Calculate modularity
    modularity_score = nx.algorithms.community.quality.modularity(graph, communities)
    return modularity_score


def draw_communities(graph: Graph, communities, pos, title=""):
    """Draw the network with node colors based on the community structure provided."""
    plt.figure(figsize=(15, 10))
    # Map each node to the community index
    community_map = {node: cid for cid, community in enumerate(communities) for node in community}
    # Colors
    colors = [community_map.get(node, 0) for node in graph.nodes()]
    nx.draw_networkx(graph, pos=pos, node_color=colors, with_labels=True, node_size=50, cmap=plt.cm.jet)
    plt.title(title)
    plt.show()


if __name__ == '__main__':
    # Filtration parameters
    N = 300
    blocks = 5
    prs = 0.02
    prr_values = [0, 0.16, 1.0]

    # True communities distribution
    nodes_per_set = 60
    true_communities = [frozenset(str(j) for j in range(1 + i * nodes_per_set, 1 + (i + 1) * nodes_per_set)) for i in
                        range(blocks)]

    # Convert prr_values to a regex-friendly string format
    prr_regex = '|'.join(f'{prr:.2f}' for prr in prr_values)  # Formats all floats to two decimal places
    # Regex pattern
    pattern = f'^synthetic_network_N_{N}_blocks_{blocks}_prr_({prr_regex})_prs_{prs:.2f}.net$'
    # Compile the regex
    compiled_pattern = re.compile(pattern)

    # Load the networks
    current_directory = os.getcwd()
    files_directory = os.path.join(current_directory, "networks")

    for filename in os.listdir(files_directory):
        # Skip files that don't match the pattern
        if not compiled_pattern.match(filename):
            # print(f"Skipping file: {filename}")
            continue

        # Extract the specific parameters from the filename, to determine the precise network configuration
        pattern = r'synthetic_network_N_(\d+)_blocks_(\d+)_prr_([\d.]+)_prs_([\d.]+)\.net'
        match = re.match(pattern, filename)
        N = int(match.group(1))
        blocks = int(match.group(2))
        prr = float(match.group(3))
        prs = float(match.group(4))
        print()
        print(f"Processing file: {filename} with parameters N: {N}, blocks: {blocks}, prr: {prr}, prs: {prs}")

        # Load the network
        graph = load_network(os.path.join(files_directory, filename))
        pos = nx.spring_layout(graph)
        print(f'Network loaded: {graph.number_of_nodes()} nodes, {graph.number_of_edges()} edges')

        # Infomap community detection -> dict[node, community]
        infomap_communities = list(infomap_detect_communities(graph).values())
        # print(f'Infomap Communities: {infomap_communities}')
        infomap_modularity_score = calculate_community_modularity(graph, infomap_communities)
        print("Infomap Modularity:", infomap_modularity_score)
        infomap_nmi_score = calculate_nmi(infomap_communities, true_communities)
        infomap_nvi_score = calculate_nvi(infomap_communities, true_communities)
        infomap_ari_score = calculate_ari(infomap_communities, true_communities)
        infomap_jaccard_score = community_jaccard_index(infomap_communities, true_communities)
        print(f"Infomap NMI Score: {infomap_nmi_score}")
        print(f"Infomap NVI Score: {infomap_nvi_score}")
        print(f"Infomap ARI Score: {infomap_ari_score}")
        print(f"Infomap Jaccard Score: {infomap_jaccard_score}")
        draw_communities(graph, infomap_communities, pos, "Infomap Community Structure with PRR={}".format(prr))

        # Greedy Modularity community detection -> list[set[node]]
        greedy_modularity_communities = greedy_modularity_detect_communities(graph)
        # print(f'Greedy Modularity Communities: {greedy_modularity_communities}')
        greedy_modularity_score = modularity(graph, greedy_modularity_communities)
        print("Greedy Modularity Modularity:", greedy_modularity_score)
        greedy_modularity_nmi_score = calculate_nmi(greedy_modularity_communities, true_communities)
        greedy_modularity_nvi_score = calculate_nvi(greedy_modularity_communities, true_communities)
        greedy_modularity_ari_score = calculate_ari(greedy_modularity_communities, true_communities)
        greedy_modularity_jaccard_score = community_jaccard_index(greedy_modularity_communities, true_communities)
        print(f"Greedy Modularity NMI Score: {greedy_modularity_nmi_score}")
        print(f"Greedy Modularity NVI Score: {greedy_modularity_nvi_score}")
        print(f"Greedy Modularity ARI Score: {greedy_modularity_ari_score}")
        print(f"Greedy Modularity Jaccard Score: {greedy_modularity_jaccard_score}")
        draw_communities(graph, greedy_modularity_communities, pos,
                         "Greedy Modularity Community Structure with PRR={}".format(prr))

        # Louvain community detection -> list[set[node]]
        louvain_communities = list(louvain_detect_communities(graph))
        # print(f'Louvain Communities: {louvain_communities}')
        louvain_modularity_score = calculate_community_modularity(graph, louvain_communities)
        print("Louvain Modularity:", louvain_modularity_score)
        louvain_nmi_score = calculate_nmi(louvain_communities, true_communities)
        louvain_nvi_score = calculate_nvi(louvain_communities, true_communities)
        louvain_ari_score = calculate_ari(louvain_communities, true_communities)
        louvain_jaccard_score = community_jaccard_index(louvain_communities, true_communities)
        print(f"Louvain NMI Score: {louvain_nmi_score}")
        print(f"Louvain NVI Score: {louvain_nvi_score}")
        print(f"Louvain ARI Score: {louvain_ari_score}")
        print(f"Louvain Jaccard Score: {louvain_jaccard_score}")
        draw_communities(graph, louvain_communities, pos, "Louvain Community Structure with PRR={}".format(prr))
