import infomap
from networkx import Graph


def infomap_detect_communities(graph: Graph) -> dict:
    # Initialize Infomap with the two-level partitioning flag
    im = infomap.Infomap("--two-level --silent")

    # Add edges to Infomap, making sure to convert node identifiers consistently
    for source, target, data in graph.edges(data=True):
        weight = data.get('weight', 1.0)  # Use the provided weight or default to 1.0
        im.add_link(int(source), int(target), weight)

    # Add isolated nodes to Infomap if any
    for node in graph.nodes():
        im.add_node(int(node))

    # Run the Infomap community detection algorithm
    im.run()

    # Create a dictionary to hold community assignments with community numbers as keys
    communities = {}
    for node in im.tree:
        if node.is_leaf:
            if node.module_id not in communities:
                communities[node.module_id] = set()
            communities[node.module_id].add(str(node.node_id))  # Add node to the appropriate community set

    return communities
