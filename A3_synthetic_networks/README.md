The objective of this activity is to understand how community detection algorithms work and some of the recurrent 
nuances appearing when using these tools to characterize the mesoscale of complex networks. The activity is divided 
into two different tasks.

Characterization of the community structure of networks with block structure

In this part of the activity, you will use community detection algorithms to analyze the community structure of 
networks generated according to the stochastic block model (SBM). The parameters characterizing the networks are:

        N: number of nodes. For all the networks, N=300.
        nblocks: number of different blocks (types) of nodes. For all the networks, nblocks=5
        prs: Probability that a node (belonging to a given block) establishes a link with another node from a different block. For all networks here studied, prs=0.02
        prr: Probability that a single node (belonging to a given block) establishes a link with another node from the same block. This indicator varies across networks, being its value indicated in each filename. For example, for the synthetic network in file ‘synthetic_network_N_300_blocks_5_prr_0.04_prs_0.02.net’, prr=0.04. 

You should use at least three algorithms to find the community structure of the different networks provided in the dataset, i.e., as prr varies from prr=0 (no connections inside each block) to prr=1 (all-to-all interactions inside blocks). Out of these three algorithms, one should be Infomap whereas the other two should be modularity maximization algorithms, e.g., Girvan-Newman, Agglomerative Greedy algorithm, Louvain, Leiden, etc. Note that, for all the synthetic networks, the ‘true’ group structure is known. Namely, the first 60 nodes (labeled as 1, 2, 3, …,60) are contained in group 1, the next 60 (labeled 60, 62, ..., 120) in group 2, and so on. 

The report must include:

    Evolution of the number of communities and the modularity of the partitions found by each algorithm as a function of prr. You should also compare the partition found by each algorithm with the ‘true’ partition using at least the following standard measures: Jaccard Index, Normalized Mutual Information (arithmetic normalization), and Normalized Variation of Information. It is not necessary to implement the calculation of these indices, you may use any program. Beware that you may find implementations of the Jaccard Index that are not related to community detection, thus the results would be incorrect.
    A color-coded visualization of the community structure of the network for prr=0.02, prr=0.16, and prr=1.00. To facilitate the comparison of partitions, you should set the position of the nodes in the network by applying any of the algorithms available for representation (e.g., Kamada-Kawai, ForceAtlas, Fruchterman-Reingold, etc.) to the network with prr=1.00. The positions obtained for each node should be kept when representing the other two networks. In each network, the node's color should identify the community assigned by each community detection algorithm. 

The network layout obtained should resemble this one:

 

    A brief discussion on the limitations of the use of modularity to classify whether a network has community structure or not. For instance, from your results, can we state that a network with Q=0.4 has community structure? Why?
    A brief discussion of the differences observed across algorithms. Are the communities detected by both of them equal? Are the modularity values obtained equal? Why?

Characterization of the community structure of real networks

In this part of the activity, you should use algorithms relying on modularity maximization to analyze the community structure of a real network capturing face-to-face interactions in a primary school in France. In this network, nodes represent either students or teachers and weights are proportional to the time they were together during the two days in which interactions were measured. More information on the network can be found here: 

http://www.sociopatterns.org/publications/high-resolution-measurements-of-face-to-face-contact-patterns-in-a-primary-school/

You should analyze both the unweighted (‘primaryschool_u.net’) and the weighted (‘primaryschool_w.net’) versions of this network. In addition, node metadata is available (metadata_primary_school.txt), indicating the school group each individual belongs to.

The report must include:

    A comparison between the community structure found in both the unweighted and the weighted networks. Use a color-coded representation, similar to the one explained in the previous activity. The position of the nodes should be set by applying the chosen positioning algorithm to the weighted network. Warning: If you choose the Kamada-Kawai layout algorithm, the weights introduced in the algorithm should be the inverse (1/wij) of the actual weights (wij) of the network.
    The composition of the detected communities in terms of the school groups to which their individuals belong. You should provide a visual representation (e.g. a stacked bar plot, a pie chart) to show how many individuals of each school group are in each community.
    A brief discussion on the differences among the communities detected in the weighted and unweighted network. Why weights are relevant?
