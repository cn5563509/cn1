import numpy as np
from sklearn.metrics import mutual_info_score
from sklearn.metrics import normalized_mutual_info_score, adjusted_rand_score


def jaccard_index(set1, set2):
    """Jaccard Index
    This index measures the similarity between two sets. It is defined as the size of the intersection divided by the
    size of the union of the sample sets.
    """
    intersection = len(set1.intersection(set2))
    union = len(set1.union(set2))
    return intersection / union if union != 0 else 0


def community_jaccard_index(communities1, communities2) -> float:
    jaccard_scores = []
    for com1 in communities1:
        scores = [jaccard_index(com1, com2) for com2 in communities2]
        jaccard_scores.append(max(scores) if scores else 0)
    return sum(jaccard_scores) / len(jaccard_scores)


def calculate_nmi(communities1, communities2) -> float:
    """Normalized Mutual Information (NMI)
    NMI is based on mutual information (MI) but normalized to scale the results between 0 (no mutual information)
    and 1 (perfect correlation).
    """
    # Convert communities to labels
    labels_true = get_labels_from_communities(communities1)
    labels_pred = get_labels_from_communities(communities2)
    return normalized_mutual_info_score(labels_true, labels_pred)


def get_labels_from_communities(communities):
    labels = {}
    for idx, community in enumerate(communities):
        for node in community:
            labels[node] = idx
    return [labels[node] for node in sorted(labels)]


def calculate_ari(communities1, communities2) -> float:
    """Adjusted Rand Index (ARI)
    ARI adjusts the Rand Index for the chance grouping of elements, providing a value between -1 and 1, where 1
    indicates perfect agreement between two clusters.
    """
    # Convert communities to labels as with NMI
    labels_true = get_labels_from_communities(communities1)
    labels_pred = get_labels_from_communities(communities2)
    return adjusted_rand_score(labels_true, labels_pred)


def entropy(labels):
    """Calculate the entropy of a label distribution."""
    if not labels:
        return 0
    label_probabilities = np.bincount(labels) / len(labels)
    nonzero_probs = label_probabilities[np.nonzero(label_probabilities)]
    return -np.sum(nonzero_probs * np.log(nonzero_probs))


def variation_of_information(X_labels, Y_labels):
    """Calculate the variation of information between two clusterings."""
    total_entropy_X = entropy(X_labels)
    total_entropy_Y = entropy(Y_labels)
    mutual_info = mutual_info_score(X_labels, Y_labels)
    return total_entropy_X + total_entropy_Y - 2 * mutual_info


def calculate_nvi(communities1, communities2):
    """Calculate the normalized variation of information."""
    X_labels = get_labels_from_communities(communities1)
    Y_labels = get_labels_from_communities(communities2)
    VI = variation_of_information(X_labels, Y_labels)
    H_X = entropy(X_labels)
    H_Y = entropy(Y_labels)
    return VI / (H_X + H_Y) if (H_X + H_Y) != 0 else 0


def get_labels_from_communities(communities):
    """Assign a unique label to each node based on its community."""
    labels = {}
    for idx, community in enumerate(communities):
        for node in community:
            labels[node] = idx
    sorted_nodes = sorted(labels)
    return [labels[node] for node in sorted_nodes]
