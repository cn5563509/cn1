import networkx as nx


# Function to load the synthetic network
def load_network(full_path: str) -> nx.Graph:
    graph = nx.Graph()

    # Open and read the file
    with open(full_path, 'r') as file:
        mode = None  # Track whether we're reading vertices or edges

        for line in file:
            if line.startswith('*'):  # Check what section we are in
                if 'vertices' in line:
                    mode = 'vertices'
                elif 'edges' in line:
                    mode = 'edges'
                continue

            if mode == 'vertices':
                # Parse vertex lines: "<index> <label> <x> <y> <shape>"
                parts = line.split()
                if len(parts) < 5:
                    continue  # Skip malformed lines
                index, label, x, y, shape = parts
                # Add vertex to graph
                graph.add_node(index, label=label, pos=(float(x), float(y)), shape=shape)

            elif mode == 'edges':
                # Parse edge lines: "<from> <to> <weight>"
                parts = line.split()
                if len(parts) < 3:
                    continue  # Skip malformed lines
                source, target, weight = parts
                # Add edge to graph
                graph.add_edge(source, target, weight=float(weight))

    return graph
